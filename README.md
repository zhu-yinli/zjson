一个封装了json-lib的工具。
使用方法：

1 字符串转换成Java对象（POJO）:

```
    JSONUtils<AccessTokenBean> jsonUtils = new JSONUtils<>(AccessTokenBean.class);
    AccessTokenBean accessTokenBean = jsonUtils.getJsonBean(jsonStr);
```

2 Java对象转换成字符串：
````
    JSONUtils<AccessTokenBean> jsonUtils = new JSONUtils<>(AccessTokenBean.class);
    String jsonStr = jsonUtils.getJsonStr(accessTokenBean);
````
3 复杂对象的处理（Java对象中含有List，Map，Array等对象）：
````
    JSONUtils<AccessTokenBean> jsonUtils = new JSONUtils<>(AccessTokenBean.class);
    
    Map<String, Class> classMap = new HashMap<>();
    classMap.put("userList", UserInfoForListBean.class);
    
    AccessTokenBean accessTokenBean = jsonUtils.getComplexJsonBean(jsonStr, classMap);
````